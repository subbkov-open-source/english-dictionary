<?php

/**
 * v1
 *
 * Class Core
 */
class Core
{

    private $urlEn = "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q={text}&tl=En-gb";

    private $urlRu = "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q={text}&tl=Ru-gb";

    private $patchTmp = "tmp/";

    private $patchTemplate = "template/";

    private $patchTask = "task/";

    private $patchResults = "results/";


    private $templatesMp3 = [
        "start" => "start.mp3",
        "end"   => "end.mp3",
        "empty" => "empty.mp3",
    ];

    private $sleep = 2;


    /**
     * @param $text
     *
     * @return string
     */
    private function getUrlEn($text)
    {
        return str_replace("{text}", urlencode(trim($text)), $this->urlEn);
    }

    private function getUrlRu($text)
    {
        return str_replace("{text}", urlencode(trim($text)), $this->urlRu);
    }


    /**
     * @param        $text
     * @param string $type
     *
     * @return mixed|string
     */
    private function getObjMp3($text, $type = "en")
    {
        $url    = "";
        $objMp3 = '';
        switch ($type) {
            case "en":
                $url = $this->getUrlEn($text);
                break;

            case "ru":
                $url = $this->getUrlRu($text);
                break;
        }

        if ($type == 'en') {
            $objMp3 = $this->getCambridgeMp3($text);
            if (empty($objMp3)) {
                $objMp3 = $this->getCurlMp3($url);
            }
        } else {
            $objMp3 = $this->getCurlMp3($url);
        }

        sleep($this->sleep);

        return $objMp3;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function getCurlMp3(string $url): string
    {
        $objMp3 = "";
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl, CURLOPT_USERAGENT, "Bot 1.0");
            $objMp3 = curl_exec($curl);
            curl_close($curl);
        }

        return $objMp3;
    }


    private function getCambridgeMp3($text): string
    {
        $objMp3    = '';
        $originUrl = 'https://dictionary.cambridge.org';
        $url       = 'https://dictionary.cambridge.org/pronunciation/english/' . urlencode(trim($text));

        $cambridgeHtml = $this->getCurlMp3($url);

        if (empty($cambridgeHtml)) {
            return $objMp3;
        }

        preg_match_all("#data-src-mp3=\"(.*?)\"#is", $cambridgeHtml, $items);

        if (count($items[1]) == 0) {
            return $objMp3;
        }

        foreach ($items[1] as $item) {
            $item   = trim($item, "'\"");
            $objMp3 = $this->getCurlMp3($originUrl . $item);
            if (!empty($objMp3)) {
                break;
            }
        }

        $ttt = 1;

        // бла бла бла - и тут парсим раздербанимаем и выклёвываем
        return $objMp3;
    }


    /**
     * @param $objMp3
     * @param $fileName
     */
    private function saveMp3($objMp3, $fileName)
    {
        $filePatch = $this->patchTmp . $fileName;
        if (!file_exists($filePatch)) {
            $fp = fopen($filePatch, "w");
            fwrite($fp, $objMp3);
            fclose($fp);
            chmod($filePatch, 777);
        }
    }


    /**
     * @param $file
     *
     * @return array
     */
    private function getWordsTask($file)
    {
        $file = file($this->patchTask . $file, FILE_SKIP_EMPTY_LINES);

        $result = [];
        foreach ($file as $key => $line) {
            list($wordsRuLine, $wordsEnLine) = explode("|", $line);
            $wordsRu = explode(",", $wordsRuLine);
            $wordsRu = array_map('trim', $wordsRu);
            foreach ($wordsRu as $word) {
                $result[$key]["ru"][$word . ".txt"] = $word;
            }
            $wordsEn = explode(",", $wordsEnLine);
            $wordsEn = array_map('trim', $wordsEn);
            foreach ($wordsEn as $word) {
                $result[$key]["en"][$word . ".txt"] = $word;
            }
        }

        return $result;
    }


    /**
     * @return array
     */
    private function getAllTasks()
    {
        $tasks = glob($this->patchTask . "/*.txt");

        foreach ($tasks as &$task) {
            $task = str_replace($this->patchTask . "/", "", $task);
        }

        return $tasks;
    }


    /**
     * @return null|string
     */
    private function getTodayTask()
    {
        $tasks = $this->getAllTasks();

        $templateToday = date("d_m_Y") . ".txt";

        if (in_array($templateToday, $tasks)) {
            return $templateToday;
        }

        return null;
    }


    /**
     * @return array
     */
    private function saveTmpMp3s()
    {
        $wordChains    = [];
        $templateToday = $this->getTodayTask();
        if ($templateToday) {
            $words = $this->getWordsTask($templateToday);
            if ($words) {
                foreach ($words as $keyLine => $lineWord) {
                    foreach ($lineWord as $languageKey => $language) {
                        foreach ($language as $id => $word) {
                            $objMp3 = $this->getObjMp3($word, $languageKey);
                            if (empty($objMp3)) {
                                sleep(5);
                                $objMp3 = $this->getObjMp3($word, $languageKey);
                                if (empty($objMp3)) {
                                    sleep(5);
                                    $objMp3 = $this->getObjMp3($word, $languageKey);
                                }
                            }
                            $this->saveMp3($objMp3, $id);
                            $wordChains[$keyLine][$languageKey][] = $id;
                        }
                    }
                }
            }
        }

        return $wordChains;
    }

    private function makeOneChain($lineWords)
    {
        $idFile = "";

        if ($lineWords) {
            $idFile            = mt_rand(1000, 9999);
            $editedLineWords[] = $this->patchTemplate . $this->templatesMp3["start"];

            $countKeyLineWords = count($lineWords) - 1;

            foreach ($lineWords as $key => $fileKey) {
                $editedLineWords[] = $this->patchTmp . $fileKey;
                if ($key != $countKeyLineWords) {
                    $editedLineWords[] = $this->patchTemplate . $this->templatesMp3["empty"];
                    $editedLineWords[] = $this->patchTemplate . $this->templatesMp3["empty"];
                    $editedLineWords[] = $this->patchTemplate . $this->templatesMp3["empty"];
                }
            }

            $editedLineWords[] = $this->patchTemplate . $this->templatesMp3["end"];

            $Oobj = "";

            foreach ($editedLineWords as $keyLine => $file) {
                $Oobj .= file_get_contents($file);
            }

            file_put_contents($this->patchTmp . $idFile . ".txt", $Oobj);
        }

        return $idFile . ".txt";
    }


    /**
     * @return array
     */
    private function makeAllChains()
    {
        $linesWords = [];
        $wordChains = $this->saveTmpMp3s();
        if ($wordChains) {
            foreach ($wordChains as $lineWords) {
                $arr          = [];
                $arr          = array_merge($arr, $lineWords["en"]);
                $arr          = array_merge($arr, $lineWords["ru"]);
                $arr          = array_merge($arr, $lineWords["en"]);
                $linesWords[] = $this->makeOneChain($arr);
            }
//
//
//
//            foreach ($wordChains as $languageKey => $language) {
//                $arr = [];
//                foreach($language as $languageKey => $lineWords){
//                    $arr = array_merge($arr, $lineWords);
//                }
//                $linesWords[] = $this->makeOneChain($arr);
//            }
        }

        return $linesWords;
    }


    public function makeMp3Today()
    {
        $format = '.mp3';
        $file_name    = date("d_m_Y") ;

        $mp3FilePatch = $this->patchResults . $file_name;
        $tmpMp3s      = $this->makeAllChains();

        $Oobj = "";

        foreach ($tmpMp3s as $keyLine => $file) {
            $patchOno = $this->patchResults . '/'. $file_name . '/';
            $patch = $patchOno. $keyLine. $format;
            $Oobj .= file_get_contents($this->patchTmp . $file);
            $mp3 = file_get_contents($this->patchTmp . $file);

            mkdir($patchOno, 0777, true) && !is_dir($patchOno);


            file_put_contents($patch, $mp3);
        }

        file_put_contents($mp3FilePatch.$format, $Oobj);

        foreach (glob($this->patchTmp . "/*.txt") as $fileDel) {
            unlink($fileDel);
        }

        if (file_exists($mp3FilePatch)) {
            $file = $mp3FilePatch;
            header("Content-Type: audio/mpeg");
            header("Accept-Ranges: bytes");
            header("Content-Length: " . filesize($file));
            header("Content-Disposition: attachment; filename=" . $file_name);
            readfile($file);
        }
    }

//
//
//
//после получения масива слов
//дать им уникальные айди и распарсить их в мп3 в темповую папку
//
//
//из готовых мпз получить 1 файл мп3 сохранить его в папку результатов, удалить темповые мп3 и вернуть на скачку файл

}
