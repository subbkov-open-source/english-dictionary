<?php


$file_name = date("d_m_Y") . ".mp3";
$mp3FilePatch = "results/".$file_name;

if (file_exists($mp3FilePatch)) {
    $file = $mp3FilePatch;
    header ("Content-Type: audio/mpeg");
    header ("Accept-Ranges: bytes");
    header ("Content-Length: ".filesize($file));
    header ("Content-Disposition: attachment; filename=".$file_name);
    readfile($file);
}