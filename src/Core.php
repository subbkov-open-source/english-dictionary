<?php declare(strict_types=1);

namespace SubbkovOpenSource\EnglishDictionary;

class Core
{
    /** @var array */
    private $params = [
        'e:' => 'execute:',
    ];

    /** @var string */
    private $execute;

    /**
     * Core constructor.
     */
    public function __construct()
    {
        $this->prepareOptions();
    }

    private function prepareOptions(): void
    {
        $options       = getopt(implode('', array_keys($this->params)), $this->params);
        $this->execute = $options['execute'] ?? $options['e'] ?? false;
    }


    public function test(){

    }

}