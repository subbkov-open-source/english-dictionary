<?php declare(strict_types=1);

namespace SubbkovOpenSource\EnglishDictionary;

use GuzzleHttp\Client;

class ParserCambridge
{

    /** @var string */
    private $url = 'https://dictionary.cambridge.org/pronunciation/english/';

    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => $this->url]);
    }

    /**
     * @param string $word
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWord(string $word)
    {
        $response = $this->client->request('GET', $word);

        $ttt = 1;
    }

}